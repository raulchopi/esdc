<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Talon
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">

	<div class="store">
		<a href="<?php site_url() ?>tienda">
			<img src="<?php bloginfo('template_directory'); ?>/images/store.png" alt="">
		</a>
	</div>
	<div class="free">
		<a href="<?php site_url() ?>tienda">
			<img src="<?php bloginfo('template_directory'); ?>/images/free.png" alt="">
		</a>
	</div>

</aside><!-- #secondary -->
